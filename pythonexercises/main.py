import os

from pythonexs import *

terminate_prog = False

def runExercise1():
	ex1 = exercise1.Exercise1()
	ex1.run()

def runExercise2():
	ex2 = exercise2.Exercise2()
	ex2.run()

def runExercise3():
	ex3 = exercise3.Exercise3()
	ex3.run()

def runExercise4():
	ex4 = exercise4.Exercise4()
	ex4.run()

def runExercise5():
	ex5 = exercise5.Exercise5()
	ex5.run()

def runExercise6():
	ex6 = exercise6.Exercise6()
	ex6.run()

def runExercise7():
	ex7 = exercise7.Exercise7()
	ex7.run()

def runExercise8():
	ex8 = exercise8.Exercise8()
	ex8.run()

def clear():
	os.system('clear')
	printMenu()

def terminate():
	global terminate_prog
	terminate_prog = True
	print "+++++ PROGRAM TERMINATED +++++"

options = {
	1 : runExercise1,
	2 : runExercise2,
	3 : runExercise3,
	4 : runExercise4,
	5 : runExercise5,
	6 : runExercise6,
	7 : runExercise7,
	8 : runExercise8,
	9 : clear
}

def printMenu():
	mn = menu.Menu()
	mn.printMenu()


def runMain():
	clear()

	print " "

	while terminate_prog != True:
		try:
			choice = int(raw_input (">>Enter choice : "))
		except Exception, e:
			print "Invalid input. Please try again. "+str(e)
			choice = -1
		finally:
			if isinstance(choice,int) == True:
				if choice >= 0 and choice <= 9:
					print " "
					options.get(choice,terminate)()
					print " "
				else:
					print "Please enter 0-9 only."
			else:
				print "Invalid input. Please try again. "





runMain()
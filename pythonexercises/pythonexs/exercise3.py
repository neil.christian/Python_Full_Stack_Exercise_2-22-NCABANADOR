class Exercise3():

	raw_list = []
	processed_list =[]

	def populateList(self):

		try:
			self.raw_list = input("Enter list of words (ex format([\"word1\",\"word2\"])) :")
		except Exception, e:
			print "There is an error in input. Please try again and follow sample format. "
			self.populateList()
		finally:
			if isinstance(self.raw_list,list) != True:
				print "There is an error in input. Please try again and follow sample format. "
				self.populateList()
			else:
				for value in self.raw_list:
					if isinstance(value, str) != True:
						print "There is an error in input. Please try again and follow sample format. "
						self.populateList()

	def removeAdjacentSameChar(self):
		for word in self.raw_list:
			index = 0
			processed_word = ""

			while index < len(word): 
				current_char = word[index]
				word_temp = word

				index2 = index + 1
				while index2 < len(word):
					if word[index2].lower == current_char.lower:
						index2 += 1
						if index2 >= len(word):
							word = word_temp[:index] + word_temp[(index2):]
							index -= 1
							index2 += 1
							break
					elif (index2-1) != index:
						word = word_temp[:index] + word_temp[(index2):]
						index -= 1
						index2 += 1
						break
					else:
						index2 += 1
						break

				index += 1
				processed_word = word

			self.processed_list.append(processed_word)

	def displayOutput(self):
		print "List with adjacent same character removed: " + str(self.processed_list)

	def run(self):
		self.populateList()
		self.removeAdjacentSameChar()
		self.displayOutput()

import random
import os

class Exercise7():

	word_list = []
	random_index = 0
	random_word = ""

	def readWordFile(self):
		current_dir = str(os.getcwd())
		file_dir = current_dir + "/pythonexs"
		word_file = open(os.path.join(file_dir, "wordlist.txt"),"r")

		for line in word_file:
			line = line.strip()
			self.word_list.append(line)

		word_file.close()

	def getRandomIndexAndWord(self):
		self.random_index = random.randint(0,len(self.word_list) - 1)
		self.random_word = self.word_list[self.random_index]

	def shuffleLetters(self,word):
		raw_word = word
		word_shuffled =""

		while len(word) != 0:
			word_temp ="";
			random_index = random.randint(0,len(word) - 1)
			word_shuffled += word[random_index]
			word_temp = word[:random_index]

			if random_index < len(word) - 1:
				word_temp += word[(random_index+1):]
			else:
				word_temp = word_temp

			word = word_temp


		print "Shuffled Word: " + word_shuffled

		self.play(raw_word,word_shuffled)


	def play(self,word_raw,word_shuffled):
		guess_counter = 0
		guessed_correctly = False

		while guessed_correctly != True and guess_counter < 3:
			word_guess = raw_input("Your Guess : ")

			if word_guess.lower() == word_raw.lower():
				guessed_correctly = True
				print "(Congrats you've guessed the word correctly.) "
			else:
				guess_counter += 1
				if guess_counter == 1 :
					print "(Mistake : "+str(guess_counter) +" , 2 more mistakes before program terminates.)"
				elif guess_counter == 2:
					print "(Mistakes : "+str(guess_counter) +" , 1 more mistake before program terminates.)"
				else:
					print "(Sorry you reach the guess limit. The correct word is : "+word_raw+")"

	def run(self):
		self.readWordFile()
		self.getRandomIndexAndWord()
		self.shuffleLetters(self.random_word)

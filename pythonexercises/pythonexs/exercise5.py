
class Exercise5():

	fibo_range = 0
	fibo_series = ""

	def inputRange(self):
		try:
			self.fibo_range = input("Number of Fibo Series: ")
		except Exception, e:
			print "Invalid input. Please try again. "+str(e)
			self.inputRange()
		finally:
			if isinstance(self.fibo_range,int) != True:
				print "Invalid input. Please try again. "
				self.inputRange()
		

	def generateFiboSeries(self,fibo_range):
		counter = 0
		fibo1 = 0
		fibo2 = 0

		while counter < self.fibo_range:
			fibo_sum = 0;
			if counter == 0:
				self.fibo_series += str(counter);
			elif counter == 1:
				fibo2 = counter
				self.fibo_series += ' '+ str(fibo2)
			else:
				fibo_sum = fibo1 + fibo2
				fibo1 = fibo2
				fibo2 = fibo_sum;
				self.fibo_series += ' ' + str(fibo_sum)

			counter += 1

	def displayOutput(self):
		print "Output: " + self.fibo_series

	def run(self):
		self.inputRange()
		self.generateFiboSeries(self.fibo_range)
		self.displayOutput()
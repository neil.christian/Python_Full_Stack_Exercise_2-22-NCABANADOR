from functools import partial
from collections import deque

class Exercise6():

	list_stack_queue = []
	choice = ""
	command=""

	def appender(self,value_to_add, use_list):
		return use_list.append(value_to_add)

	def popper(self,use_list,ds_type):
		if len(use_list) <= 0:
			print "(There is nothing to pop!)"
		elif ds_type == "Stack":
			return use_list.left()
		elif ds_type == "Queue":
			return use_list.pop()

	#def inputOption(self):


	def run(self):
		terminate = False


		print "Stack and Queue Data Structure"

		while terminate != True:
			print "Options : [1] Try Stack , [2] Try Queue , [3] Exit "
			self.choice = str(self.inputStringWithValidator("Selected Option",self.choice))
			is_stack = True
			ds_type =""

			print self.choice

			if self.choice == "1":
				self.list_stack_queue = []
				ds_type = "Stack"
				is_stack = True
			elif self.choice == "2":
				self.list_stack_queue = deque([])
				ds_type = "Queue"
				is_stack = False
			elif self.choice == "3":
				print "(Stack and Queue Terminated)"
				terminate = True 
				break
			else:
				print "(Choice must be from 1 to 3 only)"


			change_ds = False

			print ds_type +" Commands : [1] Append, [2] Pop , [3] Stop "+ds_type
			append = partial(self.appender)
			pop = partial(self.popper)
			
			while change_ds != True and terminate != True:
				self.command = str(self.inputStringWithValidator("Selected Command",self.command))
				if self.command == "1":
					value = raw_input("Input value to append : ")
					append(value, self.list_stack_queue)
					print ds_type + " : "+str(self.list_stack_queue)
				elif self.command == "2":
					pop(self.list_stack_queue)
					print ds_type + " : "+ str(self.list_stack_queue)
				elif self.command == "3":
					change_ds = True
				else:
		 			print "(Command must be from 1 to 3 only)"

	def inputStringWithValidator(self,desc,string):
		try:
			string = raw_input((str(desc) +" : "))

		except Exception, e:
			print "(There is something wrong with your input. Try Again.)"
			self.inputStringWithValidator(desc)
		finally:
			if len(string.strip()) == 0:
				print "("+str(desc)  +" is required. )"
				self.inputStringWithValidator(desc,string)
			elif desc == "Selected Option"  and string.strip() != "1" and string.strip() != "2" and string.strip() != "3":
				print "(Please enter 1 to 3 only)"
				self.inputStringWithValidator(desc,string)
			elif desc == "Selected Command"  and string.strip() != "1" and string.strip() != "2" and string.strip() != "3":
				print "(Please enter 1 to 3 only)"
				self.inputStringWithValidator(desc,string)
			else:
				return string

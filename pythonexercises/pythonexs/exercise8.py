class Exercise8():
	integer = 0
	choice = ""
	user_list =[]
	entry_username = ""
	entry_password = ""
	user_found = False
	user_customer = ""

	def __init__(self):
		print "---- ONLINE SHOPPING ----"

	def run(self):
		print ""
		print " [1] Login [2] Sign up"

		self.choice =""


		self.choice = self.inputStringWithValidator("Selected Choice")

		if self.choice == "1":
			
			self.login()
		else:
			self.signup()


	def login(self):
		print ""
		print "<<LOGIN>>"
		entry_username = self.inputStringWithValidator("Username")
		entry_password = raw_input("Password :")

		user_index = 0
		for user in self.user_list:
			if user.username == entry_username and user.password == entry_password:
				self.user_found = True
				break
			user_index += 1


		if len(self.user_list) == 0:
			print "There are no user in the list. Please signup."
			self.run()
		elif self.user_found == True:
			print "Login Success"
			self.startShopping(self.user_list[user_index])
		else:
			print "User not found. Please sign up. "
			self.run()

	def startShopping(self,customer):
		print ""
		print "---Start Shopping---"
		print "Shopping module is unfinished."
		print ""

		list_cart = []

		inventory = self.populateInventory()

		terminate = False
		print_product = True

		while terminate == False:

			if print_product == True:
				product_index = 1
				print "List of products :"
				print "------------------------------------------------"

				for product in inventory.getProductList():
					print "["+str(product_index)+"] Name: "+ product.getName() +" Price: "+ str(product.getPrice())
					product_index += 1
					print_product = False

				print "------------------------------------------------"
				print ""

			print "Command: [1] Add Item to Cart [2] Check out "

			choice = raw_input("Enter command:")

			if choice == "1" or choice == 1:
				
				try:
					product_ind = input("Enter product index :")
				except Exception, e:
					print "(Invalid product index.)"
				finally:
					if isinstance(product_ind,int) == False :
						print "(Invalid product index.)"
					elif int(product_ind) > len(inventory.getProductList()) or int(product_ind) <= 0:
						print "(Product index not found.)"
					else:
						list_cart.append(inventory.getProductList()[product_ind-1])


			elif choice == "2" or choice == 2:
				print "RECEIPT: "
				print "Customer Name : " + customer.getName()
				total = 0

				for cart in list_cart:
					print "Item: "+ cart.getName() +" Price : "+str(cart.getPrice())
					total = total + cart.getPrice()

				print "TOTAL : "+ str(total)


				print ""
				print " --Program Terminated-- "

				terminate = True
			else: 
				print "(Please input 1 or 2 only.)"




	def signup(self):
		print ""
		print "<<SIGN UP>>"

		customer = Customer()
		customer.setName(self.inputStringWithValidator("Name"))
		customer.setAge(self.inputIntWithValidator("Age"))
		self.integer = 0
		customer.setGender(str(self.inputStringWithValidator("Gender (M/F)")).upper())
		customer.setNationality(self.inputStringWithValidator("Nationality"))
		customer.setUsername(self.inputStringWithValidator("Username"))
		customer.setPassword(self.inputStringWithValidator("Password"))

		self.user_list.append(customer)

		self.run()

	def inputStringWithValidator(self,desc):
		string = ""
		try:
			string = raw_input((str(desc) +" : "))

		except Exception, e:
			print "(There is something wrong with your input. Try Again.)"
			self.inputStringWithValidator(desc)
		finally:
			if len(string.strip()) == 0:
				print "("+str(desc)  +" is required. )"
				self.inputStringWithValidator(desc)
			elif desc == "Password" and len(string.strip()) < 6:
				print "(Password must be greater than 6 characters in length.)"
				self.inputStringWithValidator(desc)
			elif desc == "Selected Choice"  and string.strip() != "1" and string.strip() != "2":
				print "(Please enter 1 or 2 only)"
				self.inputStringWithValidator(desc)
			elif desc == "Gender (M/F)":
				if string.strip().upper() == "M" or string.strip().upper() == "F":
					return string
				else:
					print "(Input for Gender Field is M or F only.)"
					self.inputStringWithValidator(desc)
			else:
				return string

	def inputIntWithValidator(self,desc):

		try:
			self.integer = int(input((str(desc) +" : ")))
		except Exception, e:
			print "(Error in input. Please enter an integer.)"
			self.inputIntWithValidator(desc)
		finally:
			if isinstance(self.integer,int) != True:
				print "(Error in input. Please enter an integer.)"
				self.inputIntWithValidator(desc)
			elif self.integer == 0:
				print "("+str(desc) + " must be greater than 0.)"
				self.inputIntWithValidator(desc)
			else:
				return self.integer

	def populateInventory(self):
		inventory = Inventory()

		phone_list = []

		apple = Product()
		apple.setName("Apple Phone")
		apple.setPrice(1000.00)
		phone_list.append(apple)

		samsung = Product()
		samsung.setName("Samsung Phone")
		samsung.setPrice(900.00)
		phone_list.append(samsung)

		nokia = Product()
		nokia.setName("Nokia Phone")
		nokia.setPrice(800.00)
		phone_list.append(nokia)

		sony = Product()
		sony.setName("Sony Phone")
		sony.setPrice(700.00)
		phone_list.append(sony)

		asus = Product()
		asus.setName("Asus Phone")
		asus.setPrice(600.00)
		phone_list.append(asus)

		inventory.setProductList(phone_list)

		return inventory

			


#Encapsulation
class Person():
	
	def __init__(self):
		self.name = ""
		self.age = 0
		self.gender = ""
		self.nationality = ""

	def setName(self,name):
		Person.name = name

	def getName(self):
		return Person.name

	def setAge(self,age):
		self.age = age

	def getAge(self):
		return self.age

	def setGender(self,gender):
		self.gender = gender

	def getGender(self):
		return self.gender

	def setNationality(self,nationality):
		self.nationality = nationality

	def getNationality(self):
		return self.nationality



#Inheritance
class Customer(Person):
	def __init__(self):
		self.username = ""
		self.password =""

	def setPassword(self,password):
		self.password = password

	def getPassword(self):
		return self.password

	def setUsername(self,username):
		self.username = username

	def getUserName(self):
		return self.username

class Product():

	def __init__(self):
		self.name = ""
		self.price =0.00

	def setName(self,name):
		self.name = name

	def getName(self):
		return self.name

	def setPrice(self,price):
		self.price = price

	def getPrice(self):
		return self.price


#Encapsulation
class Inventory():
	product_list = []

	def getProductList(self):
		return self.product_list

	def setProductList(self,list):
		self.product_list = list

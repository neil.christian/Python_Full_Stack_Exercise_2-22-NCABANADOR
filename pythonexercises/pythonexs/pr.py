#prob 5

fibo_range = input("Number of Fibo Series: ")

def generateFiboSeries(fibo_range):
	counter = 0
	fibo1 = 0
	fibo2 = 0
	fibo_series = "";

	while counter < fibo_range:
		fibo_sum = 0;
		if counter == 0:
			fibo_series += str(counter);
		elif counter == 1:
			fibo2 = counter
			fibo_series += ' '+ str(fibo2)
		else:
			fibo_sum = fibo1 + fibo2
			fibo1 = fibo2
			fibo2 = fibo_sum;
			fibo_series += ' ' + str(fibo_sum)

		counter += 1

	print fibo_series

generateFiboSeries(fibo_range)
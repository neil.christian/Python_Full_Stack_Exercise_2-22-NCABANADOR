class Exercise4():

	starting_balance = 0.00
	new_balance = 0
	interest_rate = 0.0
	growth_percentage = 0.0
	number_of_years = 0

	def inputStartingBal(self):
		try:
			self.starting_balance = float(raw_input("Enter starting balance: "))
		except Exception, e:
			print "Invalid starting balance format. Please try again. " +str(e)
			self.inputStartingBal()
		finally:
			if self.isInputFloat(self.starting_balance) != True:
				print "Invalid input. Please try again. "
				self.inputStartingBal()

	def inputNumOfYears(self):
		try:
			self.number_of_years = input("Enter number of years : ")
		except Exception, e:
			print "Invalid input. Please try again. " +str(e)
			self.inputNumOfYears()
		finally:
			if self.isInputInteger(self.number_of_years) != True:
				print "Invalid input. Please try again. "
				self.inputNumOfYears()

	def inputInterestRate(self):
		try:
			self.interest_rate = float(raw_input("Enter interest rate (%): "))
		except Exception, e:
			print "Invalid interest rate format. Please try again. "+str(e)
			self.inputInterestRate()
		finally:
			if self.isInputFloat(self.interest_rate) != True:
				print "Invalid interest rate format. Please try again. "
				self.inputInterestRate()

	def populateVariables(self):
		self.inputStartingBal()
		self.inputNumOfYears()
		self.inputInterestRate()

	def computeGrowth(self):
		interest_value_yearly = self.starting_balance * (self.interest_rate / 100.)

		self.new_balance = self.starting_balance + (interest_value_yearly * self.number_of_years)
		self.growth_percentage = self.interest_rate * self.number_of_years

	def displayOutput(self):
		print "*************************************************"
		print "New Balance : " + str(self.new_balance)
		print "Growth Percentage (%) :  " + str(self.growth_percentage)
		print "*************************************************"

	def isInputInteger(self,user_input):
		if isinstance(user_input,int) == True:
			return True
		else:
			return False

	def isInputFloat(self,user_input):
		if isinstance(user_input,float) == True:
			return True
		else:
			return False

	def run(self):
		self.populateVariables()
		self.computeGrowth()
		self.displayOutput()

class Exercise1():

	base_number = 0

	def inputN(self):
		try:
			self.base_number = input("Enter N : ")
		except Exception, e:
			print "Error in input. Please enter an integer. "
			self.inputN()
		finally:
			if isinstance(self.base_number,int) != True:
				print "Error in input. Please enter an integer. "
				self.inputN()
			else:
				if self.base_number < 2:
					print "N must be greater than or equals to 2 to form a diamond."
					self.inputN()

	def printDiamond(self):
		self.printUpper() 
		self.printCenter()
		self.printLower()
	
	def printUpper(self):
		counter_space = self.base_number
		counter_char = 1
		while counter_space > 1 :
			print (counter_space * ' ') + (('*' + ' ') * counter_char)
			counter_space -= 1
			counter_char += 1

	def printCenter(self):
		print ' ' + (('*' + ' ') * self.base_number)

	def printLower(self):
		counter_space = 2;
		counter_char = self.base_number - 1
		while counter_space <= self.base_number :
			print (counter_space * ' ') + (('*' + ' ') * counter_char)
			counter_space += 1
			counter_char -= 1

	def run(self):
		self.inputN()
		self.printDiamond()
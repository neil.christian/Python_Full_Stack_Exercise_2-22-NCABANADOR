class Exercise2():

	list_raw = []
	list_sequences = []
	strs = ""

	def inputList(self):
		
		try:
			self.list_raw = input("Enter list (ex format[1,2,3,4]) : ")
		except Exception, e:
			print "Error in input. Please try again. "
			self.inputList()
		finally:
			if isinstance(self.list_raw,list) != True:
				print "Input is not a list. Please follow example format. "
				self.inputList()
			

	def generateSequence(self,list_raw,index):
	
		if index >= len(list_raw) - 1:
			self.list_sequences.append(list_raw)

			return

		counter = index
		while counter < len(list_raw):
			temp = list_raw[index]
			list_raw[index] = list_raw[counter]
			list_raw[counter] = temp

			self.generateSequence(list_raw[:],index + 1 )

			temp = list_raw[index]
			list_raw[index] = list_raw[counter]
			list_raw[counter] = temp

			counter +=1

	def displayOutput(self):
		print "Output: " +str(self.list_sequences)

	def run(self):
		self.inputList()
		self.generateSequence(self.list_raw,0)
		self.displayOutput()




import os

class Menu():
	
	def printMenu(self):
		current_dir =  str(os.getcwd())
		file_dir = current_dir + "/pythonexs"
		menu_txt_file = open(os.path.join(file_dir,"Instructions.txt"),"r")

		for word in menu_txt_file:
			print str(word.strip())

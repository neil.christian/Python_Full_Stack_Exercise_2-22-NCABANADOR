from bson.objectid import ObjectId

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username):
        self.conn.insert({'post':post, 'username': username})

    def deletePost(self, postid,username):
    	self.conn.remove({'_id': ObjectId(str(postid)), 'username': username})

    def updateEmoticon(self,postid,emoticon,username):
    	self.conn.update({'_id': ObjectId(str(postid))},{'$set':{'emoticon':emoticon}})

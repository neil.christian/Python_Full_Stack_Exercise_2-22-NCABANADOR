from flask import Flask
from flask import g

from modules import default
from modules import users
from modules import inventory
from modules import cart

from pymongo import MongoClient

app = Flask(__name__)

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.productNeil
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.inventorydb = inventory.InventoryDB(conn=mainDb.inventory)
    g.cartdb = cart.CartDB(conn=mainDb.cart)
    
@app.teardown_request
def teardown_request(exception):
    postdb = getattr(g, 'postdb', None)
    if postdb is not None:
        postdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()

app.register_blueprint(default.mod)
app.register_blueprint(inventory.mod, url_prefix="/inventory")
app.register_blueprint(cart.mod,url_prefix="/cart")
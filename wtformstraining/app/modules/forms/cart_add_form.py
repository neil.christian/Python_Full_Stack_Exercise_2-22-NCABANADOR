from wtforms import Form
from wtforms import IntegerField
from wtforms import TextField
from wtforms import FloatField
from wtforms import validators
from wtforms import HiddenField

class CartAddForm(Form):
	product_id = HiddenField()
	product_name = TextField('Product Name: ', [validators.Length(min=1, max=25),validators.Required()])
	product_price = FloatField('Product Price', [validators.Required('Input must be a number')])
	quantity = IntegerField('Quantity', [validators.Required('Input must be a number')])

    
from wtforms import Form
from wtforms import FloatField
from wtforms import TextField
from wtforms import PasswordField
from wtforms import validators

class InventoryAddForm(Form):
    product_name = TextField('Product Name: ', [validators.Length(min=1, max=25)])
    product_price = FloatField('Product Price: ', [validators.Required('Input must be a number')])
    
from bson.objectid import ObjectId
from flask import g

class CartDB:

    def __init__(self, conn):
        self.conn = conn

    def getProductList(self,username):
        return self.conn.find({'username':username})

    def getProductById(self, product_id):
        return self.conn.find({'_id': ObjectId(str(product_id))})

    def moveProductToCart(self,product_id,quantity,username):
        inventory_quantity = 0

        is_existing_cart = False

        inventory_products = g.inventorydb.getProductById(product_id)

        for product in inventory_products:
            inventory_quantity = int(product['quantity'])
            break

        cart_products = self.getProductById(product_id)

        for product in cart_products:
            if inventory_quantity <= (quantity + product['quantity']):
                new_quantity = inventory_quantity
                new_subtotal = (float(new_quantity) * float(product['product_price']))
            else:
                new_quantity = quantity + product['quantity']
                new_subtotal = (float(quantity) * float(product['product_price'])) + float(product['subtotal'])

            self.conn.update({'_id': ObjectId(str(product_id))},{'$set':{'quantity':new_quantity,'subtotal':new_subtotal}})

            is_existing_cart = True
            break


        if is_existing_cart == False:

            inventory_products = g.inventorydb.getProductById(product_id)
            for product in inventory_products:
                if inventory_quantity <= (quantity + product['quantity']):
                    quantity = inventory_quantity

                product_name = product['product_name']
                product_price = product['product_price']
                subtotal = int(quantity) * float(product_price)

                self.conn.insert({'_id':ObjectId(str(product_id)),'product_name':product_name, 'product_price': product_price, 'quantity' : quantity,'subtotal':subtotal,'username':username})

        g.inventorydb.removeProductsFromInventory(product_id,quantity,username)


    def removeProductsFromCart(self, product_id,username):
        cart_products = self.getProductById(product_id)



        for product in cart_products:
            g.inventorydb.moveProductToInventory(product_id,product['quantity'],username)
            self.conn.remove({'_id': ObjectId(str(product_id)), 'username': username})

            break






from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session, Flask
from datetime import datetime
from ..forms import cart_add_form

mod = Blueprint('cart', __name__)

@mod.route('/')
def cart_list():
	cart = g.cartdb.getProductList(session['username'])
	current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	return render_template('carttemplate/cart_list.html', server_time=current_time,cart = cart)

@mod.route('/cart_add_product', methods=['POST','GET'])
def cart_add_product():

	form = cart_add_form.CartAddForm(request.form,request.form['product_id'])

	if request.method == 'POST' and form.validate():
		g.cartdb.moveProductToCart(form.product_id.data,form.quantity.data,session['username'])
		return redirect("/cart/")

	current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	return render_template('carttemplate/cart_addproduct.html', server_time=current_time,form = form,product_id = request.form['product_id'])

@mod.route('/cart_remove_product', methods=['POST','GET'])
def cart_remove_product():

	product_id = request.form['product_id']
	g.cartdb.removeProductsFromCart(product_id,session['username'])
	return redirect("/cart/")



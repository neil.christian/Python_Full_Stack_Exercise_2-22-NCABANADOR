from bson.objectid import ObjectId
from flask import g

class InventoryDB:

    def __init__(self, conn):
        self.conn = conn

    def getProductList(self,username):
    	return self.conn.find({'username':username})

    def getProduct(self, product_name,product_price,username):
        return self.conn.find({'product_name':product_name,'product_price':product_price,'username':username})

    def getProductById(self, product_id):
        return self.conn.find({'_id': ObjectId(str(product_id))})

    def addProduct(self, product_name,product_price,quantity,username):
        product = self.getProduct(product_name,product_price,username)
        
        product_id = ""
        is_existing = False

        for pro in product:
            product_id = pro['_id']
            quantity = pro['quantity'] + 1
            is_existing = True
            
            break

        if is_existing == True:
            self.conn.update({'_id': ObjectId(str(product_id))},{'$set':{'quantity':quantity}})
        else:
            self.conn.insert({'product_name':product_name, 'product_price': product_price, 'quantity' : 1,'username':username})

    def removeProductsFromInventory(self,product_id,quantity_to_remove,username):

        products = self.getProductById(product_id)

        for product in products:
            stock = int(product['quantity'])

            new_quantity = stock - int(quantity_to_remove)

            if new_quantity <= 0 :
                self.conn.remove({'_id': ObjectId(str(product_id)), 'username': username})
            else :
                self.conn.update({'_id': ObjectId(str(product_id))},{'$set':{'quantity':new_quantity}})

            break

    def moveProductToInventory(self,product_id,quantity,username):
        inventory_quantity = 0

        is_existing_inventory = False

        inventory_products = self.getProductById(product_id)

        for product in inventory_products:
            new_quantity = int(quantity) + int(product['quantity'])
            
            self.conn.update({'_id': ObjectId(str(product_id))},{'$set':{'quantity':new_quantity}})

            is_existing_inventory = True
            break

        if is_existing_inventory == False:
            cart_products = g.cartdb.getProductById(product_id)

            for product in cart_products:
                product_name = product['product_name']
                product_price = product['product_price']

                self.conn.insert({'_id':ObjectId(str(product_id)),'product_name':product_name, 'product_price': product_price, 'quantity' : quantity,'username':username})




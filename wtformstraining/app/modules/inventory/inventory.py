from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session, Flask
from ..forms import invent_add_form
from datetime import datetime

mod = Blueprint('inventory', __name__)

@mod.route('/')
def product_list():
	inventory = g.inventorydb.getProductList(session['username'])
	current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	return render_template('inventorytemplate/invent_list.html', server_time=current_time,inventory = inventory)

@mod.route('/add_product', methods=['POST','GET'])
def add_product():
	form = invent_add_form.InventoryAddForm(request.form)
	if request.method == 'POST' and form.validate():
		g.inventorydb.addProduct(form.product_name.data,form.product_price.data,0,session['username'])
		return redirect("/inventory/")

	current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	return render_template('inventorytemplate/invent_addproduct.html', server_time=current_time,form = form)

